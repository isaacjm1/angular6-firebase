import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {enableProdMode} from '@angular/core';

import { AngularFireModule } from '@angular/fire';
import { FirestoreSettingsToken } from '@angular/fire/firestore';
import { AppComponent } from './app.component';
import { ClientFormComponent } from './components/client-form/client-form.component';
import { ClientsComponent } from './components/clients/clients.component';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header/header.component';
import { FooterComponent } from './footer/footer/footer.component';
import { RouterModule, Routes } from '@angular/router';
import { KpiClientesComponent } from './components/kpi-clientes/kpi-clientes.component';

/* enableProdMode(); */

const routes: Routes = [
  { path: '', redirectTo: '/creacion-clientes', pathMatch: 'full'},
  { path: 'creacion-clientes', component: ClientFormComponent },
  { path: 'lista-clientes', component: ClientsComponent},
  { path: 'kpi-clientes', component: KpiClientesComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ClientFormComponent,
    ClientsComponent,
    HeaderComponent,
    FooterComponent,
    KpiClientesComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: [{ provide: FirestoreSettingsToken, useValue: {} }],
  bootstrap: [AppComponent]
})
export class AppModule { }
