export interface Client {
    id?: string;
    nombre?: string;
    apellido?: string;
    edad?: number;
    fec_nac?: string;
    fec_mue?: string;
}