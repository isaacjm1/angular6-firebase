import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Client } from '../models/client';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  clientsCollection: AngularFirestoreCollection<Client>;
  clients: Observable<Client[]>;
  clientDoc: AngularFirestoreDocument<Client>;

  constructor(public db: AngularFirestore) {
    //this.clients = this.db.collection('clientes').valueChanges();
    this.clientsCollection = this.db.collection('clientes');
    this.clients = this.clientsCollection.snapshotChanges().pipe(map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Client;
          data.id = a.payload.doc.id;
          return data;
        });
      }));
  }

  getClients(){
    return this.clients;
  }

  addClient(client: Client){
  this.clientsCollection.add(client);
  }

  deleteClient(client: Client){
    this.clientDoc = this.db.doc(`clientes/${client.id}`);
    this.clientDoc.delete();
  }

  updateClient(client: Client){
    this.clientDoc = this.db.doc(`clientes/${client.id}`);
    this.clientDoc.update(client);
  }
}

