import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { Client } from '../../models/client';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  clients = [];
  editingClient:  Client;
  editing: boolean = false;

  constructor(public clientService: ClientService) { }

  ngOnInit() {
    this.clientService.getClients().subscribe(clients => setTimeout(() => {
      this.clients = clients;
    }));
  }

  deleteClient(event , client){
    if(confirm('¿Esta seguro de eliminar el cliente?')){
    this.clientService.deleteClient(client);
    }
  }

  editClient(event , client){
    this.editingClient = client;
    this.editing = !this.editing;
  }

  updateClient(){
    this.editingClient.fec_mue = this.fechaMuerte(this.editingClient.edad);
    this.clientService.updateClient(this.editingClient);
    this.editingClient = {} as Client;
    this.editing = false;
  }

  fechaMuerte(edad) {
    const f = new Date();
    const strFecActual = f.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear();
    //Suponiendo que maximo una persona vive 120 años exagerando
    const anosmax = 120 - edad;
    const anosfin = f.getFullYear() + anosmax;
    const strFecMax = f.getDate() + '/' + (f.getMonth() + 1) + '/' + anosfin;

    const date1 = strFecActual || '01-01-1970';
    const date2 = strFecMax || new Date().toLocaleDateString();
    const date3 = new Date(date1).getTime();
    const date4 = new Date(date2).getTime();

    const fecFinal = new Date(Math.random() * (date4 - date3) + date3).toLocaleDateString();
    return fecFinal;
}
}
