import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';

import { Client } from '../../models/client';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit {

  client = {} as Client;
  constructor(public clientService: ClientService) { }

  ngOnInit() {
  }

  fechaMuerte(edad) {
    const f = new Date();
    const strFecActual = f.getDate() + '/' + (f.getMonth() + 1) + '/' + f.getFullYear();
    //Suponiendo que maximo una persona vive 120 años exagerando
    const anosmax = 120 - edad;
    const anosfin = f.getFullYear() + anosmax;
    const strFecMax = f.getDate() + '/' + (f.getMonth() + 1) + '/' + anosfin;

    const date1 = strFecActual || '01-01-1970';
    const date2 = strFecMax || new Date().toLocaleDateString();
    const date3 = new Date(date1).getTime();
    const date4 = new Date(date2).getTime();

    const fecFinal = new Date(Math.random() * (date4 - date3) + date3).toLocaleDateString();
    return fecFinal;
}

  addClient() {
    if (this.client.nombre !== '' && this.client.apellido !== '' && this.client.edad !== 0 && 
       this.client.fec_nac !== ''){
        this.client.fec_mue = this.fechaMuerte(this.client.edad);
        this.clientService.addClient(this.client);
        this.client = {} as Client;
      }
  }
}
