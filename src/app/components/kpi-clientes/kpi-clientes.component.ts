import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../services/client.service';
import { Client } from '../../models/client';

@Component({
  selector: 'app-kpi-clientes',
  templateUrl: './kpi-clientes.component.html',
  styleUrls: ['./kpi-clientes.component.css']
})
export class KpiClientesComponent implements OnInit {

  clients = [];
  npromEdades = 0;
  ncantEdades = 0;
  nsumEdades = 0;
  nrango = 0;
  dvarianza = 0.00;
  ddesEstandar = 0.00;
  

  constructor(public clientService: ClientService) { }

  ngOnInit() {
    this.clientService.getClients().subscribe(clients => setTimeout(() => {
      this.clients = clients;
    }));
  }

  promEdades() {
    this.nsumEdades = 0;
    this.ncantEdades = this.clients.length;
    if (this.ncantEdades > 0) {
    for (const client of this.clients) {
      this.nsumEdades += client.edad;
   }
      this.npromEdades = this.nsumEdades / this.ncantEdades;
      return Math.round((this.npromEdades + Number.EPSILON) * 100) / 100;
    } else {
      return 0;
    }
  }

  desEstandar() {
    this.ddesEstandar = 0;
    this.dvarianza = 0;
    this.ncantEdades = this.clients.length;
    if (this.npromEdades > 0) {
      for (const client of this.clients) {
        this.nrango = Math.pow((client.edad - this.npromEdades), 2);
        this.dvarianza += this.nrango;
     }
        this.dvarianza = this.dvarianza / this.ncantEdades;
        this.ddesEstandar = Math.sqrt(this.dvarianza);
        return Math.round((this.ddesEstandar + Number.EPSILON) * 100) / 100;
      } else {
        return 0;
      }
  }
}
