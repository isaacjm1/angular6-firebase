export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC71yEcNjvMPVqIdeTKSflUbPF4VcJhUeg',
    authDomain: 'angular6-firebase04062020.firebaseapp.com',
    databaseURL: 'https://angular6-firebase04062020.firebaseio.com',
    projectId: 'angular6-firebase04062020',
    storageBucket: 'angular6-firebase04062020.appspot.com',
    messagingSenderId: '141004261455',
    appId: '1:141004261455:web:ca34c34d3ffb675a70823a',
    measurementId: 'G-7TBB1XZTNF'
  }
};
