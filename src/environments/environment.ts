// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyC71yEcNjvMPVqIdeTKSflUbPF4VcJhUeg',
    authDomain: 'angular6-firebase04062020.firebaseapp.com',
    databaseURL: 'https://angular6-firebase04062020.firebaseio.com',
    projectId: 'angular6-firebase04062020',
    storageBucket: 'angular6-firebase04062020.appspot.com',
    messagingSenderId: '141004261455',
    appId: '1:141004261455:web:ca34c34d3ffb675a70823a',
    measurementId: 'G-7TBB1XZTNF'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
